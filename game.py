# -*- coding: utf-8 -*-
"""
Created on Fri Oct 16 17:46:43 2020

@author: rdn04
"""

from ctypes import *

# Define the color types of robots and end positions
# R = Red
# G = Green
# B = Blue
# Y = Yellow
# X = Black
colors = {0: 'R',
          1: 'G',
          2: 'B',
          3: 'Y',
          4: 'X'}

# Define directions a robot can take
# N = North
# E = East
# S = South
# W = West
directions = {1: 'N',
              2: 'E',
              4: 'S',
              8: 'W'}

class Game(Structure):
    _fields_ = [('grid', c_uint * 256),
                ('moves', c_uint * 256),
                ('robots', c_uint * 5),
                ('token', c_uint),
                ('last', c_uint)]



    


